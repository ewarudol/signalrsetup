using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace SignalRSetUp
{
    //Hubs are transient don't store state in a property on the hub class.
    //Every hub method call is executed on a new hub instance.
    public class ChatHub : Hub
    { 
        public async Task AmazingFunctionForClient(string user, string message)
        {
            await Clients.All.SendAsync("SomeoneAmazingIsChatting", user, message);
            await Clients.Caller.SendAsync("SomeoneAmazingIsChatting", "only for you transmission", "only for you transmission");
        }
    }
}