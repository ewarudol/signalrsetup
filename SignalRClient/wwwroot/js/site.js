﻿var connection = new signalR.HubConnectionBuilder().withUrl("https://localhost:5001/chatHub", {
    skipNegotiation: true,
    transport: signalR.HttpTransportType.WebSockets
}).build();

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;

connection.on("SomeoneAmazingIsChatting", function (user, message) {
    
    var li = document.createElement("li");
    li.textContent = "user: " + user + ", msg: " + message;
    
    document.getElementById("messagesList").appendChild(li);
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    
    connection.invoke("AmazingFunctionForClient", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});